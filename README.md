# About this Repo

This is the Git repo of the semi-official Docker image for [Trisquel GNU/Linux](https://hub.docker.com/r/kpengboy/trisquel/). See
the Hub page for the full readme on how to use the Docker image and for information regarding contributing and issues.

The full readme can be found at https://hub.docker.com/repository/docker/kpengboy/trisquel.

This repository uses code which was sourced from https://github.com/scollazo/docker-brew-trisquel-debootstrap and https://github.com/tianon/docker-brew-debian.
