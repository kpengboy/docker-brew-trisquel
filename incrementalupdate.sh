#!/bin/bash
set -eo pipefail

cd "$(readlink -f "$(dirname "$BASH_SOURCE")")"

versions=( "$@" )
if [ ${#versions[@]} -eq 0 ]; then
	for dir in */; do
		if [ "$dir" != "docker/" ]; then
			versions+=( "$dir" );
		fi
	done
fi
versions=( "${versions[@]%/}" )

get_part() {
	dir="$1"
	shift
	part="$1"
	shift
	if [ -f "$dir/$part" ]; then
		cat "$dir/$part"
		return 0
	fi
	if [ -f "$part" ]; then
		cat "$part"
		return 0
	fi
	if [ $# -gt 0 ]; then
		echo "$1"
		return 0
	fi
	return 1
}

repo="$(get_part . repo '')"
if [ "$repo" ]; then
	if [[ "$repo" != */* ]]; then
		user="$(docker info | awk '/^Username:/ { print $2 }')"
		if [ "$user" ]; then
			repo="$user/$repo"
		fi
	fi
fi

latest="$(get_part . latest '')"
for version in "${versions[@]}"; do
	dir="$(readlink -f "$version")"
	suite="$(get_part "$dir" suite "$version")"
	container_name="$(mktemp -u XXXXXXXX)"

        (
        	set -x
		docker run --name "$container_name" "$repo:$suite" sh -c \
		       'apt-get update \
		        && DEBIAN_FRONTEND=noninteractive apt-get -y \
		               --no-install-recommends dist-upgrade \
		        && apt-get clean \
			&& rm -rf /var/lib/apt/lists*'
		docker export "$container_name" | xz > "$dir/rootfs.tar.xz"
		docker rm "$container_name"
                cat > "$dir/Dockerfile" <<EOF
FROM scratch
ADD rootfs.tar.xz /
CMD ["/bin/bash"]
EOF
	        docker build -t "$repo:$suite" "$dir"
	)
	docker run --rm "${repo}:${suite}" bash -xc '
		cat /etc/apt/sources.list
		echo
		cat /etc/os-release 2>/dev/null
		echo
		cat /etc/lsb-release 2>/dev/null
		echo
		cat /etc/debian_version 2>/dev/null
		true
	'
	docker run --rm "${repo}:${suite}" dpkg-query -f \
	       '${Package}\t${Version}\n' -W > "$dir/build.manifest"

	if [ "$suite" != "$version" ]; then
		( set -x && docker tag "${repo}:${suite}" "${repo}:${version}" )
	fi
	if [ "$suite" = "$latest" ]; then
		( set -x && docker tag "$repo:$suite" "$repo:latest" )
	fi
done
